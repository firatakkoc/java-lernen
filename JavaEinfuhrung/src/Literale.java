/**
 * Lernziel:
 * -Literaltypen kennenlernen:
 * - die Wahrheitswerte `true` und `false`
 * - integrale Literale für Zahlen, etwa `122`
 * - Fließkommaliterale, etwa `12.567` oder `9.999E-2`
 * - String-Literale für Zeichenketten, wie `"Mandalore"`
 * `null` steht für einen besonderen Referanztyp
 * - Hexadezimalzahlen
 * - Binäre Schreibweise
 * - Unterstriche in Zahlen
 */

public class Literale {
    public static void main(String[] args) {
        System.out.println(true);
        System.out.println(false);
        System.out.println(0.1 + 0.1+ 0.1);
        System.out.println('\n');
        System.out.println(1234);
        System.out.println("Hallo");
        System.out.println("'");
        System.out.println(0x0); // 0
        System.out.println(0x9); // 9
        System.out.println(0xa); // 10
        System.out.println(0xf); // 15
        System.out.println(0x10); // 16
        System.out.println(0b1010101001); // 681

    }
}
