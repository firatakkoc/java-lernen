package Taschenrechner;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        /*
        Schreibe ein Java-Programm, das als interaktiver Taschenrechner fungiert.
        Das Programm soll den Benutzer dazu auffordern, zwei Zahlen einzugeben und dann eine mathematische Operation auszuwählen:
        Addition, Subtraktion, Multiplikation oder Division.
        Basierend auf der ausgewählten Operation sollen die beiden Zahlen entsprechend verarbeitet und das Ergebnis ausgegeben werden.
        Anforderungen:
        Verwende die Scanner-Klasse, um die Eingaben des Benutzers zu erfassen.
        Stelle sicher, dass die eingegebenen Zahlen vom Datentyp Integer sind.
        Verwende eine Variable vom Typ char, um die ausgewählte Operation zu speichern (z.B. '+', ','*,'/*).
        Verwende eine Variable vom Typ boolean, um zu überprüfen, ob die eingegebene Operation gültig ist.
        Verwende if- und else-if-Anweisungen, um die ausgewählte Operation zu überprüfen und die entsprechende Berechnung durchzuführen.
        Verwende einen switch-case-Block, um das Ergebnis der Berechnung auszugeben, abhängig von der ausgewählten Operation.
        Überprüfe, ob der Benutzer eine ungültige Operation eingegeben hat, und gib eine entsprechende Fehlermeldung aus.
        Gib das Ergebnis der Berechnung mit einer passenden Ausgabe aus.
        Beispiel:
        Willkommen zum interaktiven Taschenrechner! Bitte gib die erste Zahl ein: 10 Bitte gib die zweite Zahl ein: 5 Welche Operation möchtest du durchführen? Wähle eine Option:
        Addition (+) Subtraktion (-) Multiplikation (*) Division (/)
        Deine Auswahl: +
         */

        Scanner scanner = new Scanner(System.in);

        System.out.println("Willkommen zum interaktiven Taschenrechner!");
        System.out.println("-------------------------------------------------");
        System.out.print("Bitte gib die erste Zahl ein: ");
        int num1 = scanner.nextInt();
        System.out.println("-------------------------------------------------");
        System.out.print("Bitte gib die zweite Zahl ein:");
        int num2 = scanner.nextInt();
        System.out.println("-------------------------------------------------");
        System.out.print("Welche Operation möchtest du durchführen? Wähle eine Option:");
        char operator = scanner.next().charAt(0);

        boolean operation = true;
        int result = 0;

        scanner.close();

        if (operator == '+') {
            result = num1 + num2;
        }
        else if (operator == '-') {
            result = num1 - num2;
        }
        else if (operator == '*') {
            result = num1 * num2;
        }
        else if (operator == '/') {
            if (num2 != 0) {
                result = num1 / num2;
                System.out.println("Fehler! Division durch null ist nicht erlaubt.");
                operation = false;
            }
            else {
                System.out.println("Fehler! Ungültige Operation.");
                operation = false;
            }
        }




        if (operation == true) {

            switch (operator) {
                case '+':
                    System.out.println("Das Ergebnis der Addition ist: " + result);
                    break;
                case '-':
                    System.out.println("Das Ergebnis der Subtraktion ist: " + result);
                    break;
                case '*':
                    System.out.println("Das Ergebnis der Multiplikation ist: " + result);
                    break;
                case '/':
                    System.out.println("Das Ergebnis der Division ist: " + result);
                    break;
            }
        }

        else {
            System.out.println("Closed");
        }





    }
}
